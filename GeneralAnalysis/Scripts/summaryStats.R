options(stringsAsFactors = F)
library(data.table)

#Cross this info to filter impaired regulatory interactions
#Get file names
setwd("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/ImpReg/")
f <- list.files(path = "/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/ImpReg/", pattern = "*_impaired_regInt",full.names = F)
#Init final df
fdf = data.frame("Source"=NA,"Target"=NA,"IntType"=NA,"Sign"=NA,"FC"=NA,"RSID"=NA,"Disease"=NA,"Pop"=NA)
#For each
for(i in seq(1,length(f))){
  tmpd = fread(f[i],header=T)
  if(nrow(tmpd)>0){
    tmpd = tmpd[,c("Source","Target","IntType","Sign","FC","RSID")]
    #Add disease info
    x <- strsplit(f[i],split="_")[[1]]
    tmpd$Disease <- x[1]
    #Add cell (sub)type info
    tmpd$Pop <- x[length(x)-2]
    if(nrow(tmpd)>0){
      #Reorder & merge to final df
      tmpd <- tmpd[,c("Source","Target","IntType","Sign","FC","RSID","Disease","Pop")]
      #Remove SNPs linked to Wolff-Parkinson-White (PD)
      if(x[1] == "PD")
      fdf <- fdf[which(!fdf$RSID %in% c("rs76351165","rs117728810","rs66628686","rs77902041")),]
      if(nrow(tmpd)>0){
        fdf <- rbind(fdf,tmpd)
      }
    }
    rm(tmpd)
  }
}
#Remove NA init
fdf <- fdf[!is.na(fdf$Source),]
#Remove duplicate
fdf <- fdf[!duplicated(fdf[,1:8]),]
#Write results
write.table(fdf,"/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/AllDiseases_ImpReg.txt",sep="\t",row.names = F,col.names = T,quote = F)

#Bind ranked TFs
f <- list.files(path = "/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/ImpReg/", pattern = "*_ranked_TFreg.txt",full.names = T)
#Init final df
fdf = data.frame("TF"=NA,"Score"=NA,"Rank"=NA,"Disease"=NA,"Pop"=NA)
#For each
for(i in seq(1,length(f))){
  tmpd = fread(f[i],header=T)
  if(nrow(tmpd)>0){
    #Add disease info
    x <- strsplit(f[i],split="_")[[1]]
    tmpd$Disease <- x[1]
    #Add cell (sub)type info
    tmpd$Pop <- x[length(x)-2]
    if(nrow(tmpd)>0){
      fdf <- rbind(fdf,tmpd)
    }
    rm(tmpd)
  }
}
#Remove NA init
fdf <- fdf[!is.na(fdf$TF),]
#Remove duplicate
fdf <- fdf[!duplicated(fdf[,1:5]),]
#Write results
write.table(fdf,"/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/AllDiseases_TFranks.txt",sep="\t",row.names = F,col.names = T,quote = F)

#Heatmaps
library(pheatmap)
library(stringr)
library(RColorBrewer)
anc <- list(
  IntType = c("TF-Promoter"="deepskyblue",
              "Enhancer-Promoter"="goldenrod1",
              "TF-Enhancer"="salmon")
)
fdf = fdf[,c("Source","Target","IntType","RSID","Disease","Pop")]
fdf <- fdf[!duplicated(fdf[,1:6]),]

#AD
fad <- fdf[which(fdf$Disease == "AD"),]
mtxAD <- get_mtximpreg(fad)
dfAnAD <- data.frame("RN"=rownames(mtxAD),"IntType"="TF-Promoter")
dfAnAD$IntType[str_detect(dfAnAD$RN,"^[A-Z].*[0-9]{5,}$")] <- "TF-Enhancer"
dfAnAD$IntType[str_detect(dfAnAD$RN,"^chr")] <- "Enhancer-Promoter"
rownames(dfAnAD) <- dfAnAD$RN
dfAnAD$RN <- NULL
png("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/SummaryStats/HeatmapAD.png",width = 8, height = 4, units = 'in', res = 300)
pheatmap(mtxAD,cellwidth=20,cellheight=10,treeheight_col=20,treeheight_row=10,annotation_row=dfAnAD,annotation_colors = anc)
dev.off()

#PD
fpd <- fdf[which(fdf$Disease == "PD"),]
mtxPD <- get_mtximpreg(fpd)
dfAnPD <- data.frame("RN"=rownames(mtxPD),"IntType"="TF-Promoter")
dfAnPD$IntType[str_detect(dfAnPD$RN,"^[A-Z].*[0-9]{5,}$")] <- "TF-Enhancer"
dfAnPD$IntType[str_detect(dfAnPD$RN,"^chr")] <- "Enhancer-Promoter"
rownames(dfAnPD) <- dfAnPD$RN
dfAnPD$RN <- NULL
png("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/SummaryStats/HeatmapPD.png",width = 10, height = 8, units = 'in', res = 300)
pheatmap(mtxPD,cellwidth=20,cellheight=10,treeheight_col=20,treeheight_row=10,annotation_row = dfAnPD,annotation_colors = anc)
dev.off()

#EPI
fEpi <- fdf[which(fdf$Disease == "EPI"),]
mtxEPI <- get_mtximpreg(fEpi)
dfAnEPI <- data.frame("RN"=rownames(mtxEPI),"IntType"="TF-Promoter")
dfAnEPI$IntType[str_detect(dfAnEPI$RN,"^[A-Z].*[0-9]{5,}$")] <- "TF-Enhancer"
dfAnEPI$IntType[str_detect(dfAnEPI$RN,"^chr")] <- "Enhancer-Promoter"
rownames(dfAnEPI) <- dfAnEPI$RN
dfAnEPI$RN <- NULL
png("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/SummaryStats/HeatmapEPI.png",width = 10, height = 8, units = 'in', res = 300)
pheatmap(mtxEPI,cellwidth=20,cellheight=10,treeheight_col=20,treeheight_row=10,annotation_row = dfAnEPI,annotation_colors = anc)
dev.off()

#T1D
ft1d <- fdf[which(fdf$Disease == "T1D"),]
mtxT1D <- get_mtximpreg(ft1d)
dfAnT1D <- data.frame("RN"=rownames(mtxT1D),"IntType"="TF-Promoter")
dfAnT1D$IntType[str_detect(dfAnT1D$RN,"^[A-Z].*[0-9]{5,}$")] <- "TF-Enhancer"
dfAnT1D$IntType[str_detect(dfAnT1D$RN,"^chr")] <- "Enhancer-Promoter"
rownames(dfAnT1D) <- dfAnT1D$RN
dfAnT1D$RN <- NULL
png("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/SummaryStats/HeatmapT1D.png",width = 8, height = 4, units = 'in', res = 300)
pheatmap(mtxT1D,cellwidth=20,cellheight=10,treeheight_col=20,treeheight_row=10,annotation_row = dfAnT1D,annotation_colors = anc)
dev.off()

#T2D
ft2d <- fdf[which(fdf$Disease == "T2D"),]
mtxT2D <- get_mtximpreg(ft2d)
dfAnT2D <- data.frame("RN"=rownames(mtxT2D),"IntType"="TF-Promoter")
dfAnT2D$IntType[str_detect(dfAnT2D$RN,"^[A-Z].*[0-9]{5,}$")] <- "TF-Enhancer"
dfAnT2D$IntType[str_detect(dfAnT2D$RN,"^chr")] <- "Enhancer-Promoter"
rownames(dfAnT2D) <- dfAnT2D$RN
dfAnT2D$RN <- NULL
png("/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/SummaryStats/HeatmapT2D.png",width = 8, height = 5, units = 'in', res = 300)
pheatmap(mtxT2D,cellwidth=20,cellheight=10,treeheight_col=20,treeheight_row=10,annotation_row = dfAnT2D,annotation_colors = anc)
dev.off()

#Function to get matrix of impaired interactions: nb = nb of SNPs impairing the interaction
get_mtximpreg <- function(fd){
  #Interactions
  fd$Int <-paste(fd$Source,fd$Target,sep="|")
  #Init matrix
  n <- length(unique(fd$Int))*length(unique(fd$Pop))
  mtx <- matrix(rep(0,n),nrow=length(unique(fd$Int)),ncol=(length(unique(fd$Pop))))
  rownames(mtx) <- unique(fd$Int)
  colnames(mtx) <- unique(fd$Pop)
  #Enhancer-promoter = nb of SNPs linked with TF-enhancer
  fdtf <- fd[which(fd$IntType == "TF-Enhancer"),]
  for(i in seq(1,nrow(mtx))){
    for(j in seq(1,ncol(mtx))){
      tmp <- fd[which(fd$Int == rownames(mtx)[i] & fd$Pop == colnames(mtx)[j]),]
      if(nrow(tmp)>0){
        if(unique(tmp$IntType) != "Enhancer-Promoter"){
          mtx[i,j] <- nrow(tmp)
        }else{
          #Remove SNPs that are not in TF-Enhancer (RNetDys updated to take this issue into account)
          tmptf <- fdtf[which(fdtf$Target == tmp$Source[1] & fdtf$Pop == tmp$Pop[1]),]
          tmp <- tmp[which(tmp$RSID %in% c(unique(tmptf$RSID))),]
          mtx[i,j] <- nrow(tmp)
        }
      }
    }
  }
  return(mtx)
}
