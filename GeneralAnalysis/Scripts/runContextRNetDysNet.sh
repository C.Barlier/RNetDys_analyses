#!/bin/sh

PATHRNEYDYS="/Users/celine.barlier/Desktop/PROJECTS/ONGOING/RNETDYS_ONGOING/RNetDysCode/"
PATHNET="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/RNetDysNet/"
PATHSNP="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/SNPs/"
OUTPATH="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/"

#AD on MDFG
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Astro_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_Astro"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Ex_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_Ex"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Inh_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_Inh"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Mic_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_Mic"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Oligo_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_Oligo"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_OPCs_S2PEreg_net.txt" -s $PATHSNP"AD_SNPs.txt" -a "hg38" -o $OUTPATH -n "AD_MDFG_OPCs"

#PD on SUNI
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_Astro_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_Astro"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_DAn_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_DAn"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_Ex_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_Ex"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_Mic_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_Mic"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_Oligo_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_Oligo"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"SUNI_OPCs_S2PEreg_net.txt" -s $PATHSNP"PD_SNPs.txt" -a "hg38" -o $OUTPATH -n "PD_SUNI_OPCs"

#EPI on MDFG
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Astro_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_Astro"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Ex_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_Ex"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Inh_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_Inh"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Mic_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_Mic"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Oligo_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_Oligo"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_OPCs_S2PEreg_net.txt" -s $PATHSNP"EPI_SNPs.txt" -a "hg38" -o $OUTPATH -n "EPI_MDFG_OPCs"

#HUN on MDFG
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Astro_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_Astro"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Ex_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_Ex"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Inh_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_Inh"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Mic_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_Mic"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_Oligo_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_Oligo"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"MDFG_OPCs_S2PEreg_net.txt" -s $PATHSNP"HUN_SNPs.txt" -a "hg38" -o $OUTPATH -n "HUN_MDFG_OPCs"

#T1D on Pancreas
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Alpha_cells_S2PEreg_net.txt" -s $PATHSNP"T1D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T1D_Alpha"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Beta_cells_S2PEreg_net.txt" -s $PATHSNP"T1D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T1D_Beta"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Delta_cells_S2PEreg_net.txt" -s $PATHSNP"T1D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T1D_Delta"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Ductal_cells_S2PEreg_net.txt" -s $PATHSNP"T1D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T1D_Ductal"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Gamma_cells_S2PEreg_net.txt" -s $PATHSNP"T1D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T1D_Gamma"

#T2D on Pancreas
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Alpha_cells_S2PEreg_net.txt" -s $PATHSNP"T2D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T2D_Alpha"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Beta_cells_S2PEreg_net.txt" -s $PATHSNP"T2D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T2D_Beta"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Delta_cells_S2PEreg_net.txt" -s $PATHSNP"T2D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T2D_Delta"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Ductal_cells_S2PEreg_net.txt" -s $PATHSNP"T2D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T2D_Ductal"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"Gamma_cells_S2PEreg_net.txt" -s $PATHSNP"T2D_SNPs.txt" -a "hg38" -o $OUTPATH -n "T2D_Gamma"

#BD on PBMC
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Bcell_progenitor_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_Bprog"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_effector_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_CD8eff"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_naive_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_CD8naive"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD16_monocytes_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_CD16mono"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_DCs_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_pDCs"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_pre-Bcells_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_preB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Breg_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_Breg"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD4memory_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_CD4memory"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_doubleNegTcells_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_bdNegTc"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_kineNK_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_kineNK"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_memB_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_memB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_naiveB_S2PEreg_net.txt" -s $PATHSNP"BD_SNPs.txt" -a "hg19" -o $OUTPATH -n "BD_naiveB"

#RA on PBMC
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Bcell_progenitor_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_Bprog"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_effector_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_CD8eff"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_naive_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_CD8naive"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD16_monocytes_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_CD16mono"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_DCs_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_pDCs"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_pre-Bcells_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_preB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Breg_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_Breg"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD4memory_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_CD4memory"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_doubleNegTcells_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_bdNegTc"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_kineNK_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_kineNK"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_memB_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_memB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_naiveB_S2PEreg_net.txt" -s $PATHSNP"RA_SNPs.txt" -a "hg19" -o $OUTPATH -n "RA_naiveB"

#SjS on PBMC
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Bcell_progenitor_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_Bprog"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_effector_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_CD8eff"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_naive_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_CD8naive"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD16_monocytes_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_CD16mono"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_DCs_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_pDCs"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_pre-Bcells_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_preB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Breg_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_Breg"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD4memory_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_CD4memory"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_doubleNegTcells_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_bdNegTc"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_kineNK_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_kineNK"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_memB_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_memB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_naiveB_S2PEreg_net.txt" -s $PATHSNP"SjS_SNPs.txt" -a "hg19" -o $OUTPATH -n "SjS_naiveB"

#SLE on PBMC
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Bcell_progenitor_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_Bprog"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_effector_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_CD8eff"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD8_naive_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_CD8naive"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD16_monocytes_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_CD16mono"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_DCs_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_pDCs"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_pre-Bcells_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_preB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_Breg_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_Breg"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_CD4memory_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_CD4memory"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_doubleNegTcells_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_bdNegTc"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_kineNK_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_kineNK"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_memB_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_memB"
sh $PATHRNEYDYS"Contextualize.sh" -g $PATHNET"PBMC_naiveB_S2PEreg_net.txt" -s $PATHSNP"SLE_SNPs.txt" -a "hg19" -o $OUTPATH -n "SLE_naiveB"
