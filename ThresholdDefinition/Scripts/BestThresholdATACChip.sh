ChIPfile="/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Prior-Knowledge/ChIPseq/hg19/sorted.final.annotated.clean.hg19.chip.bed"

scATAC_BJ="/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/scATACseq/hg19/GSE65360/sorted.GSE65360_single-BJ.peaks.bed"
scATAC_GM12878="/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/scATACseq/hg19/GSE65360/sorted.GSE65360_single-GM12878.peaks.bed"
scATAC_H1ESC="/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/scATACseq/hg19/GSE65360/sorted.GSE65360_single-H1ESC.peaks.bed"
scATAC_K562="/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/scATACseq/hg19/GSE65360/sorted.GSE65360_single-K562.peaks.bed"

#Analytic solution - PNJ paper

#Positive distribution: overlap of TP distribution

#Negative distribution: overlap of FN distribution

#K562 - 0.95
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.95 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r095.bed"
#K562 - 0.90
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.90 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r090.bed"
#K562 - 0.85
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.85 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r085.bed"
#K562 - 0.80
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.80 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r080.bed"
#K562 - 0.75
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.75 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r075.bed"
#K562 - 0.70
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.70 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r070.bed"
#K562 - 0.65
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.65 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r065.bed"
#K562 - 0.60
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.60 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r060.bed"
#K562 - 0.55
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.55 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r055.bed"
# #K562 - 0.50
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.50 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r050.bed"
#K562 - 0.45
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.45 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r045.bed"
#K562 - 0.40
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.40 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r040.bed"
#K562 - 0.35
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.35 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r035.bed"
#K562 - 0.30
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.30 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r030.bed"
#K562 - 0.25
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.25 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r025.bed"
#K562 - 0.20
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.20 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r020.bed"
#K562 - 0.15
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.15 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r015.bed"
#K562 - 0.10
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.10 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r010.bed"
#K562 - 0.05
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_K562 -b $ChIPfile -f 0.05 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_K562_r005.bed"


#H1ESC - 0.95
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.95 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r095.bed"
#H1ESC - 0.90
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.90 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r090.bed"
#H1ESC - 0.85
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.85 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r085.bed"
#H1ESC - 0.80
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.80 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r080.bed"
#H1ESC - 0.75
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.75 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r075.bed"
#H1ESC - 0.70
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.70 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r070.bed"
#H1ESC - 0.65
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.65 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r065.bed"
#H1ESC - 0.60
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.60 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r060.bed"
#H1ESC - 0.55
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.55 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r055.bed"
#H1ESC - 0.50
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.50 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r050.bed"
#H1ESC - 0.45
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.45 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r045.bed"
#H1ESC - 0.40
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.40 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r040.bed"
#H1ESC - 0.35
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.35 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r035.bed"
#H1ESC - 0.30
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.30 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r030.bed"
#H1ESC - 0.25
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.25 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r025.bed"
#H1ESC - 0.20
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.20 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r020.bed"
#H1ESC - 0.15
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.15 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r015.bed"
#H1ESC - 0.10
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.10 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r010.bed"
#H1ESC - 0.05
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_H1ESC -b $ChIPfile -f 0.05 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_H1ESC_r005.bed"


#GM12878 - 0.95
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.95 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r095.bed"
#GM12878 - 0.90
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.90 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r090.bed"
#GM12878 - 0.85
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.85 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r085.bed"
#GM12878 - 0.80
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.80 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r080.bed"
#GM12878 - 0.75
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.75 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r075.bed"
#GM12878 - 0.70
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.70 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r070.bed"
#GM12878 - 0.65
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.65 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r065.bed"
#GM12878 - 0.60
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.60 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r060.bed"
#GM12878 - 0.55
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.55 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r055.bed"
#GM12878 - 0.50
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.50 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r050.bed"
#GM12878 - 0.45
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.45 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r045.bed"
#GM12878 - 0.40
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.40 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r040.bed"
#GM12878 - 0.35
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.35 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r035.bed"
#GM12878 - 0.30
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.30 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r030.bed"
#GM12878 - 0.25
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.25 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r025.bed"
#GM12878 - 0.20
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.20 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r020.bed"
#GM12878 - 0.15
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.15 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r015.bed"
#GM12878 - 0.10
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.10 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r010.bed"
#GM12878 - 0.05
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_GM12878 -b $ChIPfile -f 0.05 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_GM12878_r005.bed"


#BJ - 0.95
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.95 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r095.bed"
#BJ - 0.90
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.90 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r090.bed"
#BJ - 0.85
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.85 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r085.bed"
#BJ - 0.80
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.80 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r080.bed"
#BJ - 0.75
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.75 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r075.bed"
#BJ - 0.70
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.70 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r070.bed"
#BJ - 0.65
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.65 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r065.bed"
#BJ - 0.60
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.60 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r060.bed"
#BJ - 0.55
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.55 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r055.bed"
#BJ - 0.50
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.50 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r050.bed"
#BJ - 0.45
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.45 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r045.bed"
#BJ - 0.40
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.40 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r040.bed"
#BJ - 0.35
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.35 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r035.bed"
#BJ - 0.30
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.30 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r030.bed"
#BJ - 0.25
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.25 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r025.bed"
#BJ - 0.20
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.20 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r020.bed"
#BJ - 0.15
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.15 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r015.bed"
#BJ - 0.10
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.10 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r010.bed"
#BJ - 0.05
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a $scATAC_BJ -b $ChIPfile -f 0.05 -r -wo -sorted > "/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Validation/Threshold_ATAC_ChIP/bedtoolIntersect_GSE65360_BJ_r005.bed"


#Run Rscript
Rscript /Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/benchmarkingThreshold.R