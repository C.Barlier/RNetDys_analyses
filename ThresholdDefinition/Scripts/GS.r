options(stringsAsFactors = F)
library(stringr)

setwd("~/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Prior-Knowledge/ChIPseq/hg19/")
data <- read.table("sorted.final.annotated.clean.hg19.chip.bed", sep = "\t", header=F)
colnames(data) <- c("Chr","Start","End","Strand","Score","TSSdist","PromoterID","Target","Antigen","Tissue","CellType","Exp")

setwd("~/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Prior-Knowledge/ChIPseq/")
tfs <- read.table("Homo_sapiens_TF.txt",sep="\t",header = T)
tfs_cofactor <- read.table("Homo_sapiens_TF_cofactors.txt",sep="\t",header = T)
n_tf <- c(tfs$Symbol,tfs_cofactor$Symbol)
n_tf <- unique(n_tf)
  
#BJ
b <- str_detect(data$CellType,"BJ")
GS_Epidermidis <- data[which(b == TRUE),]
GS_Epidermidis <- GS_Epidermidis[which(GS_Epidermidis$Antigen %in% n_tf),]
GS_Epidermidis <- GS_Epidermidis[which(GS_Epidermidis$Tissue == "Epidermis"),]
GS_Epidermidis <- GS_Epidermidis[!duplicated(GS_Epidermidis[c("Chr","Start","End","Strand","Target","Antigen")]),] #If exactly the same peak but different experiments, remove to avoid issue with benchmarking perf analysis
write.table(GS_Epidermidis,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.Epidermidis.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)


#GM12878
b <- str_detect(data$CellType,"GM12878")
GS_GM12878 <- data[which(b == TRUE),]
GS_GM12878 <- GS_GM12878[which(GS_GM12878$Antigen %in% n_tf),]
GS_GM12878 <- GS_GM12878[!duplicated(GS_GM12878[c("Chr","Start","End","Strand","Target","Antigen")]),]
write.table(GS_GM12878,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.GM12878.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)

#TF-1 - only 19 interactions = removed
b <- str_detect(data$CellType,"TF-1")
GS_TF1 <- data[which(b == TRUE),]
GS_TF1 <- GS_TF1[which(GS_TF1$Antigen %in% n_tf),]
GS_TF1 <- GS_TF1[!duplicated(GS_TF1[c("Chr","Start","End","Strand","Target","Antigen")]),]
write.table(GS_TF1,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.TF-1.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)

#K-562
b <- str_detect(data$CellType,"K-562")
GS_K562 <- data[which(b == TRUE),]
GS_K562 <- GS_K562[which(GS_K562$Antigen %in% n_tf),]
GS_K562 <- GS_K562[!duplicated(GS_K562[c("Chr","Start","End","Strand","Target","Antigen")]),]
write.table(GS_K562,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.K-562.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)

#HL-60
b <- str_detect(data$CellType,"HL-60")
GS_HL60 <- data[which(b == TRUE),]
GS_HL60 <- GS_HL60[which(GS_HL60$Antigen %in% n_tf),]
GS_HL60 <- GS_HL60[!duplicated(GS_HL60[c("Chr","Start","End","Strand","Target","Antigen")]),]
write.table(GS_HL60,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.HL-60.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)

#H1ESC
b <- str_detect(data$CellType,"ESC H1")
GS_H1ESC <- data[which(b == TRUE),]
GS_H1ESC <- GS_H1ESC[which(GS_H1ESC$Antigen %in% n_tf),]
GS_H1ESC <- GS_H1ESC[!duplicated(GS_H1ESC[c("Chr","Start","End","Strand","Target","Antigen")]),]
write.table(GS_H1ESC,"/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/GS/hg19.H1ESC.GS.bed",sep="\t",row.names=F,col.names=T,quote=F)
rm(b)

#Number of interactions for each cell line
df_s <- data.frame("Cell_line"=c("BJ","GM12878","K-562","H1ESC"),
           "Peaks"=c(length(rownames(GS_Epidermidis)),length(rownames(GS_GM12878)),length(rownames(GS_K562)),length(rownames(GS_H1ESC))),
           "Interactions"=c(length(unique(paste(GS_Epidermidis$Antigen,GS_Epidermidis$Target))),length(unique(paste(GS_GM12878$Antigen,GS_GM12878$Target))),length(unique(paste(GS_K562$Antigen,GS_K562$Target))),length(unique(paste(GS_H1ESC$Antigen,GS_H1ESC$Target)))),
           "TFs"=c(length(unique(GS_Epidermidis$Antigen)),length(unique(GS_GM12878$Antigen)),length(unique(GS_K562$Antigen)),length(unique(GS_H1ESC$Antigen))),
           "Targets"=c(length(unique(GS_Epidermidis$Target)),length(unique(GS_GM12878$Target)),length(unique(GS_K562$Target)),length(unique(GS_H1ESC$Target)))
           )

print(df_s)
