#!/usr/bin/env Rscript
options(stringsAsFactors = F)
library(data.table)
library(reshape2)
library(dplyr)
#Arguments 1: scRNAseq dataset name, 2: prefix tmp file
args = commandArgs(trailingOnly=TRUE)
#For reproducibility 
set.seed(1234)

#Variables:
RNApath="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/DATA/"
OUTpath="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/"

RNAdataset=args[1]

PipelinePathJulia="julia -p 5 /scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/"
TmpPathPIDC="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/tmp/"

#Percentage expression genes: expressed at least in 50% of the cells
PercExpVec=50

####### FUNCTIONS ########
#Function to filter a data table based on a percentage of gene expression
#Input = data.table, percentage expression minimum for which genes are removed
#Output = filtered data.table
dt_filter_gene_expression <- function(dt,PercExp){
  #Binarize data
  dtb <- dt
  dtb[dtb>0] <- 1
  #Count for each gene number of cell expressing it: percentage
  cg <- apply(dtb,1,function(x){sum(x)/length(dtb)*100})
  names(cg) <- rownames(dtb)
  #Keep only the ones stricly greater than the threshold
  cg_tokeep <- which(cg>PercExp)
  #Filter the data
  dt <- dt[cg_tokeep,]
  return(dt)
}

##########################

#Load the scRNAseq dataset
dt <- read.table(paste(RNApath,RNAdataset,sep=""),sep="\t",header=T)
#Formatting
colnames(dt)[1] <- "Gene"
dt <- dt[!duplicated(dt$Gene),] #remove potential duplicates
rownames(dt) <- dt$Gene
dt$Gene <- NULL
#Filter genes (at least in 50% of the cells)
dt_filt <- dt_filter_gene_expression(dt,PercExpVec)
#Run MI
print("MI")
#Write tmp data to use
datPath=paste(PipelinePathJulia,paste0(args[2],"tmpMI.txt"),sep="")
write.table(dt_filt,datPath,sep="\t",row.names = T,col.names = NA,quote=F)
#Create Julia command
com <- paste(paste(paste(PipelinePathJulia,"MI.jl",sep=""),datPath,sep=" "),paste(paste(OUTpath,"MI/",sep=""),paste(paste(paste(paste("TFGene","_Network_inference_MI_",sep=""),50,sep=""),"exp_",sep=""),RNAdataset,sep=""),sep=""),sep=" ")
#Run command
system(com)
#Remove tmp data
system(paste("rm",datPath))

#Save R session
sessionInfo() %>% capture.output(file=paste0(OUTpath,"MI/session_info.txt"))

