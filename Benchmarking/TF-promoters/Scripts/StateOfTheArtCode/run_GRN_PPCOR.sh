#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

## BJ
Rscript run_GRN_PPCOR.R "GSE113415_BJ_FPKM.txt"
Rscript run_GRN_PPCOR.R "GSE160910_single_count_BJ.txt"
Rscript run_GRN_PPCOR.R "GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt"
Rscript run_GRN_PPCOR.R "GSE100344_BJ_FPKM.txt"

## GM12878
Rscript run_GRN_PPCOR.R "GSE81861_GM12878_count_batch2.txt"
Rscript run_GRN_PPCOR.R "GSM4156602_GM12878_count_rep2.txt"
Rscript run_GRN_PPCOR.R "GGSM3596321_GM12878.txt"
Rscript run_GRN_PPCOR.R "GSM4156603_GM12878_count_rep3.txt"

## H1-ESC
Rscript run_GRN_PPCOR.R "GSE64016_H1ESC_normalized.txt"
Rscript run_GRN_PPCOR.R "GSE75748_H1ESC.txt"
Rscript run_GRN_PPCOR.R "GSE81861_H1ESC_count_batch1.txt"
Rscript run_GRN_PPCOR.R "GSM5534158_H1.txt"

## A549
Rscript run_GRN_PPCOR.R "GSE81861_A549_scRNAseq.txt"
Rscript run_GRN_PPCOR.R "GSM3271042_scRNAseq_A549.txt"

## Jurkat
Rscript run_GRN_PPCOR.R "Jurkat_GSE105451_scRNAseq.txt"
Rscript run_GRN_PPCOR.R "Jurkat_scRNAseq.txt"

## K-562
Rscript run_GRN_PPCOR.R "GSE113415_K562_FPKM.txt"
Rscript run_GRN_PPCOR.R "GSE81861_K562_count.txt"
Rscript run_GRN_PPCOR.R "GSE90063_k562_umi_wt.txt"
Rscript run_GRN_PPCOR.R "GSM1599500_K562_cells.txt"

conda deactivate