options(stringsAsFactors = F)
setwd("~/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/BENCHMARKING/GS_network/Cistrome/ChIPData")

#K-562
f <- read.table("K562_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("K562/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("K562/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"K562_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

rm(list = ls())

#GM12878
f <- read.table("GM12878_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("GM12878/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("GM12878/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"GM12878_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

rm(list=ls())

#H1ESC
f <- read.table("H1ESC_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("H1ESC/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("H1ESC/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"H1ESC_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

rm(list=ls())

#BJ
f <- read.table("BJ_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("BJ/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("BJ/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"BJ_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

#A549
f <- read.table("A549_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("A549/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("A549/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"A549_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

rm(list = ls())

#Jurkat
f <- read.table("Jurkat_files.txt",sep="\t",header=F)
f <- f$V1

#Merge all the bed files together for annotation with HOMER
d <- read.table(paste("Jurkat/",f[1],sep=""),sep="\t",header=F)
d$Antigen <- strsplit(f[1],split="_")[[1]][2]
d$V4 <- paste(d$V4,d$Antigen,sep="_")

for(i in seq(2,length(f))){
  tmp <- read.table(paste("Jurkat/",f[i],sep=""),sep="\t",header=F)
  tmp$Antigen <- strsplit(f[i],split="_")[[1]][2]
  tmp$V4 <- paste(tmp$V4,tmp$Antigen,sep="_")
  d <- rbind(d,tmp)
}

#remove peaks duplicates
d_filered <- d[!duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]),]
ind <- which(duplicated(d[,c("V1","V2","V3","V4","V5","Antigen")]))
(length(d_filered$V1) + length(ind)) == length(d$V1)

write.table(d_filered,"Jurkat_toAnnotate.bed",sep="\t",row.names = F,col.names = F,quote = F)

rm(list = ls())

#Annotation with HOMER - hg38
#sh runCistromeGSannotation.sh

setwd("~/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Prior-Knowledge/ChIPseq")
tfs <- read.table("Homo_sapiens_TF.txt",sep="\t",header = T)
tfs_cofactor <- read.table("Homo_sapiens_TF_cofactors.txt",sep="\t",header = T)
n_tf <- c(tfs$Symbol,tfs_cofactor$Symbol)
n_tf <- unique(n_tf)
rm(tfs)
rm(tfs_cofactor)

#BJ GS
bj_gs <- read.table("BJ_HomerAnnotated.bed",sep="\t",header=T)
bj_gs <- bj_gs[which(bj_gs$Distance.to.TSS >= -1500 & bj_gs$Distance.to.TSS <= 500),]
bj_gs <- bj_gs[,c(1,16)]
colnames(bj_gs) <- c("Source","Target")
bjs <- sapply(strsplit(bj_gs$Source,split="_"), function(x){x[2]})
bjs <- sapply(strsplit(bjs,split="-"), function(x){x[1]})
bj_gs$Source <- bjs
rm(bjs)
bj_gs <- bj_gs[!duplicated(bj_gs[,c("Source","Target")]),]

#BJ GS - TF-TF
bj_gs_tf <- bj_gs[which(bj_gs$Source %in% n_tf & bj_gs$Target %in% n_tf),]
saveRDS(bj_gs_tf,"BJ_GSnetwork_TF.rds")

#BJ GS - TF-Gene
bj_gs_tfgene <- bj_gs[which(bj_gs$Source %in% n_tf),]
saveRDS(bj_gs_tfgene,"BJ_GSnetwork_TFGene.rds")
rm(bj_gs)
rm(bj_gs_tf)
rm(bj_gs_tfgene)

#ESC H1 GS
esc_gs <- read.table("H1ESC_HomerAnnotated.bed",sep="\t",header=T)
esc_gs <- esc_gs[which(esc_gs$Distance.to.TSS >= -1500 & esc_gs$Distance.to.TSS <= 500),]
esc_gs <- esc_gs[,c(1,16)]
colnames(esc_gs) <- c("Source","Target")
escjs <- sapply(strsplit(esc_gs$Source,split="_"), function(x){x[2]})
escjs <- sapply(strsplit(escjs,split="-"), function(x){x[1]})
esc_gs$Source <- escjs
rm(escjs)
esc_gs <- esc_gs[!duplicated(esc_gs[,c("Source","Target")]),]

#ESC H1 GS - TF-TF
esc_gs_tf <- esc_gs[which(esc_gs$Source %in% n_tf & esc_gs$Target %in% n_tf),]
saveRDS(esc_gs_tf,"H1ESC_GSnetwork_TF.rds")

#ESC H1 GS - TF-Gene
esc_gs_tfgene <- esc_gs[which(esc_gs$Source %in% n_tf),]
saveRDS(esc_gs_tfgene,"H1ESC_GSnetwork_TFGene.rds")
rm(esc_gs)
rm(esc_gs_tf)
rm(esc_gs_tfgene)


#GM12878 GS
gm12878_gs <- read.table("GM12878_HOMERAnnotated.bed",sep="\t",header=T)
gm12878_gs <- gm12878_gs[which(gm12878_gs$Distance.to.TSS >= -1500 & gm12878_gs$Distance.to.TSS <= 500),]
min(gm12878_gs$Distance.to.TSS)
max(gm12878_gs$Distance.to.TSS)
gm12878_gs <- gm12878_gs[,c(1,16)]
colnames(gm12878_gs) <- c("Source","Target")
gm12878js <- sapply(strsplit(gm12878_gs$Source,split="_"), function(x){x[2]})
gm12878js <- sapply(strsplit(gm12878js,split="-"), function(x){x[1]})
gm12878_gs$Source <- gm12878js
rm(gm12878js)
gm12878_gs <- gm12878_gs[!duplicated(gm12878_gs[,c("Source","Target")]),]

#ESC H1 GS - TF-TF
gm12878_gs_tf <- gm12878_gs[which(gm12878_gs$Source %in% n_tf & gm12878_gs$Target %in% n_tf),]
saveRDS(gm12878_gs_tf,"GM12878_GSnetwork_TF.rds")

#ESC H1 GS - TF-Gene
gm12878_gs_tfgene <- gm12878_gs[which(gm12878_gs$Source %in% n_tf),]
saveRDS(gm12878_gs_tfgene,"GM12878_GSnetwork_TFGene.rds")
rm(gm12878_gs)
rm(gm12878_gs_tf)
rm(gm12878_gs_tfgene)


#K562 GS
k562_gs <- read.table("K562_HOMERAnnotated.bed",sep="\t",header=T)
k562_gs <- k562_gs[which(k562_gs$Distance.to.TSS >= -1500 & k562_gs$Distance.to.TSS <= 500),]
min(k562_gs$Distance.to.TSS)
max(k562_gs$Distance.to.TSS)
k562_gs <- k562_gs[,c(1,16)]
colnames(k562_gs) <- c("Source","Target")
K562js <- sapply(strsplit(k562_gs$Source,split="_"), function(x){x[2]})
K562js <- sapply(strsplit(K562js,split="-"), function(x){x[1]})
k562_gs$Source <- K562js
rm(K562js)
k562_gs <- k562_gs[!duplicated(k562_gs[,c("Source","Target")]),]

#ESC H1 GS - TF-TF
k562_gs_tf <- k562_gs[which(k562_gs$Source %in% n_tf & k562_gs$Target %in% n_tf),]
saveRDS(k562_gs_tf,"K562_GSnetwork_TF.rds")

#ESC H1 GS - TF-Gene
k562_gs_tfgene <- k562_gs[which(k562_gs$Source %in% n_tf),]
saveRDS(k562_gs_tfgene,"K562_GSnetwork_TFGene.rds")
rm(k562_gs)
rm(k562_gs_tf)
rm(k562_gs_tfgene)


#Jurkat GS
options(stringsAsFactors = F)
Jurkat_gs <- read.table("Jurkat_HOMERAnnotated.bed",sep="\t",header=T)
Jurkat_gs <- Jurkat_gs[which(Jurkat_gs$Distance.to.TSS >= -1500 & Jurkat_gs$Distance.to.TSS <= 500),]
min(Jurkat_gs$Distance.to.TSS)
max(Jurkat_gs$Distance.to.TSS)
Jurkat_gs <- Jurkat_gs[,c(1,16)]
colnames(Jurkat_gs) <- c("Source","Target")
Jurkatjs <- sapply(strsplit(Jurkat_gs$Source,split="_"), function(x){x[2]})
Jurkatjs <- sapply(strsplit(Jurkatjs,split="-"), function(x){x[1]})
Jurkat_gs$Source <- Jurkatjs
rm(Jurkatjs)
Jurkat_gs <- Jurkat_gs[!duplicated(Jurkat_gs[,c("Source","Target")]),]

#ESC H1 GS - TF-TF
Jurkat_gs_tf <- Jurkat_gs[which(Jurkat_gs$Source %in% n_tf & Jurkat_gs$Target %in% n_tf),]
saveRDS(Jurkat_gs_tf,"Jurkat_GSnetwork_TF.rds")

#ESC H1 GS - TF-Gene
Jurkat_gs_tfgene <- Jurkat_gs[which(Jurkat_gs$Source %in% n_tf),]
saveRDS(Jurkat_gs_tfgene,"Jurkat_GSnetwork_TFGene.rds")
rm(Jurkat_gs)
rm(Jurkat_gs_tf)
rm(Jurkat_gs_tfgene)


#A549 GS
A549_gs <- read.table("A549_HOMERAnnotated.bed",sep="\t",header=T)
A549_gs <- A549_gs[which(A549_gs$Distance.to.TSS >= -1500 & A549_gs$Distance.to.TSS <= 500),]
min(A549_gs$Distance.to.TSS)
max(A549_gs$Distance.to.TSS)
A549_gs <- A549_gs[,c(1,16)]
colnames(A549_gs) <- c("Source","Target")
A549js <- sapply(strsplit(A549_gs$Source,split="_"), function(x){x[2]})
A549js <- sapply(strsplit(A549js,split="-"), function(x){x[1]})
A549_gs$Source <- A549js
rm(A549js)
A549_gs <- A549_gs[!duplicated(A549_gs[,c("Source","Target")]),]

#ESC H1 GS - TF-TF
A549_gs_tf <- A549_gs[which(A549_gs$Source %in% n_tf & A549_gs$Target %in% n_tf),]
saveRDS(A549_gs_tf,"A549_GSnetwork_TF.rds")

#ESC H1 GS - TF-Gene
A549_gs_tfgene <- A549_gs[which(A549_gs$Source %in% n_tf),]
saveRDS(A549_gs_tfgene,"A549_GSnetwork_TFGene.rds")
rm(A549_gs)
rm(A549_gs_tf)
rm(A549_gs_tfgene)

#Table GS

tf_bj <- readRDS("TF-TF/BJ_GSnetwork_TF.rds")
tf_k562 <- readRDS("TF-TF/K562_GSnetwork_TF.rds")
tf_H1ESC <- readRDS("TF-TF/H1ESC_GSnetwork_TF.rds")
tf_GM12878 <- readRDS("TF-TF/GM12878_GSnetwork_TF.rds")
tf_Jurkat <- readRDS("TF-TF/Jurkat_GSnetwork_TF.rds")
tf_a549 <- readRDS("TF-TF/A549_GSnetwork_TF.rds")

tfgene_bj <- readRDS("TF-Gene/BJ_GSnetwork_TFGene.rds")
tfgene_k562 <- readRDS("TF-Gene/K562_GSnetwork_TFGene.rds")
tfgene_H1ESC <- readRDS("TF-Gene/H1ESC_GSnetwork_TFGene.rds")
tfgene_GM12878 <- readRDS("TF-Gene/GM12878_GSnetwork_TFGene.rds")
tfgene_Jurkat <- readRDS("TF-Gene/Jurkat_GSnetwork_TFGene.rds")
tfgene_a549 <- readRDS("TF-Gene/A549_GSnetwork_TFGene.rds")

df_tf <- data.frame("Cell_Line"=c("BJ","K-562","ESC H1","GM12878","Jurkat","A549"), "TFs_Source"=rep(NA,6), "Unique_Target"=rep(NA,6), "Interactions"=rep(NA,6))
df_tfgene <- data.frame("Cell_Line"=c("BJ","K-562","ESC H1","GM12878","Jurkat","A549"), "TFs_Source"=rep(NA,6), "Unique_Target"=rep(NA,6), "Interactions"=rep(NA,6))

df_tf$TFs_Source <- c(length(unique(tf_bj$Source)),length(unique(tf_k562$Source)),length(unique(tf_H1ESC$Source)),length(unique(tf_GM12878$Source)),length(unique(tf_Jurkat$Source)),length(unique(tf_a549$Source)))
df_tf$Unique_Target <- c(length(unique(tf_bj$Target)),length(unique(tf_k562$Target)),length(unique(tf_H1ESC$Target)),length(unique(tf_GM12878$Target)),length(unique(tf_Jurkat$Target)),length(unique(tf_a549$Target)))
df_tf$Interactions <- c(nrow(tf_bj),nrow(tf_k562),nrow(tf_H1ESC),nrow(tf_GM12878),nrow(tf_Jurkat),nrow(tf_a549))

df_tfgene$TFs_Source <- c(length(unique(tfgene_bj$Source)),length(unique(tfgene_k562$Source)),length(unique(tfgene_H1ESC$Source)),length(unique(tfgene_GM12878$Source)),length(unique(tfgene_Jurkat$Source)),length(unique(tfgene_a549$Source)))
df_tfgene$Unique_Target <- c(length(unique(tfgene_bj$Target)),length(unique(tfgene_k562$Target)),length(unique(tfgene_H1ESC$Target)),length(unique(tfgene_GM12878$Target)),length(unique(tfgene_Jurkat$Target)),length(unique(tfgene_a549$Target)))
df_tfgene$Interactions <- c(nrow(tfgene_bj),nrow(tfgene_k562),nrow(tfgene_H1ESC),nrow(tfgene_GM12878),nrow(tfgene_Jurkat),nrow(tfgene_a549))


View(df_tf)
View(df_tfgene)

