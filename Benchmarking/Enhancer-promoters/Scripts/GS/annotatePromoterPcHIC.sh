CHIPFILE="chip.promoter.regions.hg19.bed" #light version of chipseq

PATH=$PATH:/Users/celine.barlier/Desktop/PROJECTS/GRN_INTEGRATIVE_APPROACH_2021/Tools/samtools-1.11/
PATH=$PATH:/Users/celine.barlier/Documents/bedtools2/bin/


#ENHANCER REGION - Frag2: Keep if enhancer fall completely inside an enhancer region from GeneHancer
sort -k1,1 -k2,2n "GeneHancerCoordinates.txt" > "sorted.GeneHancerCoordinates.txt"
sort -k1,1 -k2,2n "enhancerRegion.hg19.H1.po.txt" > "sorted.enhancerRegion.hg19.H1.po.txt"
sort -k1,1 -k2,2n "enhancerRegion.hg19.GM.po.txt" > "sorted.enhancerRegion.hg19.GM.po.txt"
sort -k1,1 -k2,2n "enhancerRegion.hg19.IMR90.po.txt" > "sorted.enhancerRegion.hg19.IMR90.po.txt"

/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.enhancerRegion.hg19.H1.po.txt" -b "sorted.GeneHancerCoordinates.txt" -F 1.0 -wo -sorted > "toKeep.enhancerRegion.hg19.H1.po.txt"

/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.enhancerRegion.hg19.GM.po.txt" -b "sorted.GeneHancerCoordinates.txt" -F 1.0 -wo -sorted > "toKeep.enhancerRegion.hg19.GM.po.txt"

/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.enhancerRegion.hg19.IMR90.po.txt" -b "sorted.GeneHancerCoordinates.txt" -F 1.0 -wo -sorted > "toKeep.enhancerRegion.hg19.IMR90.po.txt"



sort -k1,1 -k2,2n "toAnnotate.promoterRegion.hg19.H1.po.txt" > "sorted.toAnnotate.promoterRegion.hg19.H1.po.txt"
sort -k1,1 -k2,2n "toAnnotate.promoterRegion.hg19.GM.po.txt" > "sorted.toAnnotate.promoterRegion.hg19.GM.po.txt"
sort -k1,1 -k2,2n "toAnnotate.promoterRegion.hg19.IMR90.po.txt" > "sorted.toAnnotate.promoterRegion.hg19.IMR90.po.txt"

#PROMOTER REGION - Frag1: Peak completely falling inside the promoter region
/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.toAnnotate.promoterRegion.hg19.H1.po.txt" -b $CHIPFILE -F 1.0 -wo -sorted > "promoterAn.hg19.H1.po.txt"

/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.toAnnotate.promoterRegion.hg19.GM.po.txt" -b $CHIPFILE -F 1.0 -wo -sorted > "promoterAn.hg19.GM.po.txt"

/Users/celine.barlier/Documents/bedtools2/bin/bedtools intersect -a "sorted.toAnnotate.promoterRegion.hg19.IMR90.po.txt" -b $CHIPFILE -F 1.0 -wo -sorted > "promoterAn.hg19.IMR90.po.txt"





