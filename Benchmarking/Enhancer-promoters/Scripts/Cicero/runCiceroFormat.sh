#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base

OUTPUTPATH="/scratch/users/cbarlier/GRN2021/Cicero/"

sort -k1,1 -k2,2n $OUTPUTPATH"GeneHancerCoordinates.txt" > $OUTPUTPATH"sorted.GeneHancerCoordinates.bed"
sort -k1,1 -k2,2n $OUTPUTPATH"chip.promoter.regions.hg19.bed" > $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed"

#Run script to get genes found in scRNA-seq to make a fair comparison with RNetDys
Rscript getGenes.R

## GM12878

#SAVE NAME
GM_PN1=$OUTPUTPATH"GM_d1_Cicero_net.txt"
GM_PN2=$OUTPUTPATH"GM_d2_Cicero_net.txt"

Rscript formatCiceroToEP.R $OUTPUTPATH"GM_d1.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"GMgenes.rds" $GM_PN1
Rscript formatCiceroToEP.R $OUTPUTPATH"GM_d2.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"GMgenes.rds" $GM_PN2


## H1-ESC

#SAVE NAME
H1_PN1=$OUTPUTPATH"H1_d1_Cicero_net.txt"
H1_PN2=$OUTPUTPATH"H1_d2_Cicero_net.txt"

Rscript formatCiceroToEP.R $OUTPUTPATH"H1_d1.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"H1genes.rds" $H1_PN1
Rscript formatCiceroToEP.R $OUTPUTPATH"H1_d2.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"H1genes.rds" $H1_PN2

## BJ

#SAVE NAME
BJ_PN1=$OUTPUTPATH"BJ_d1_Cicero_net.txt"
BJ_PN2=$OUTPUTPATH"BJ_d2_Cicero_net.txt"

Rscript formatCiceroToEP.R $OUTPUTPATH"BJ_d1.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"BJgenes.rds" $BJ_PN1
Rscript formatCiceroToEP.R $OUTPUTPATH"BJ_d2.rds" $OUTPUTPATH $OUTPUTPATH"sorted.GeneHancerCoordinates.bed" $OUTPUTPATH"sorted.chip.promoter.regions.hg19.bed" $OUTPUTPATH"BJgenes.rds" $BJ_PN2

conda deactivate